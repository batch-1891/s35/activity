const express = require('express')

const dotenv = require('dotenv')

const mongoose = require('mongoose')

const app = express()

const port = 3001


dotenv.config()

mongoose.connect(`mongodb+srv://admin123:${process.env.MONGODB_PASSWORD}@zuitt-bootcamp.cuxdw.mongodb.net/?retryWrites=true&w=majority`, 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
)

let db = mongoose.connection

db.on('error', console.error.bind(console, "Connection Error"))
db.on('open', () => console.log('Connected to MongoDB!'))

// CREATING THE ROUTES
app.use(express.json()) 
app.use(express.urlencoded({extended: true}))

const userSchema = new mongoose.Schema({
	username : String,
	password : String
})

const User = mongoose.model('User', userSchema)


app.post('/signup', (request, response) => {

	User.findOne({ username: request.body.username },{ password: request.body.password }, (error, result) => {
		// Check for duplicates
		if(result != null && result.username == request.body.username) {
			return response.send('Duplicate user found!')

		// If no duplicates
		} else {
			// Check if the username and the password contains a string, if it is both empty the code will go to the else statement
			if(request.body.username !== '' && request.body.password !== ''){
				// If both username and password contains value, then continue with the process of saving that user as a new user in the database collection
				let newUser = new User({
					username: request.body.username,
					password: request.body.password
				})

				newUser.save((error, savedUser) => {
					if(error){
						return response.send(error)
					}

					return response.send('New user has been registered!')
				})
			} else {
				return response.send('BOTH Username AND Password must be provided.')
			}
		}
	})
})

app.listen(port, () => console.log(`Server is running at port ${port}`))



















